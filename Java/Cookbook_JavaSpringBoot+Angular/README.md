# Cookbook - recipe app
Simple app to add recipes and corresponding images to the database.

## Table of contents
* [Technologies](#technologies)
* [Setup](#setup)
* [Usage](#usage)
* [Repository](#repository)
* [Roadmap](#roadmap)

## Technologies
Project is created with:
* Java 8
* Spring Boot (2.7.11)
* Angular (^15.2.0)
* Bootstrap (^5.2.3)
* HTML5
* CSS3
* JavaScript
* Node (~18.15.0)
* MySQL (latest)
* Git

## Setup
To set up the project, follow these steps:

1. Install Angular if you haven't already done so. You can find instructions on how to do this at angular.io.
2. Clone the repository to your local machine.
3. Open your favorite IDE and navigate to the Backend folder. Start the BackendApplication.java file.
4. Open a separate terminal or command prompt window, navigate to the Frontend folder, and run npm install.
5. After the installation has completed, run npm start to start the frontend application.
6. Open a web browser and navigate to http://localhost:4200 to access the application.

Note: If you encounter any issues during the setup process, please check the documentation for your specific IDE or Angular installation.

## Usage
To use the recipe application, follow these steps:

1. Fill out the details of the recipe, including the title, ingredients, and instructions.
2. Choose the corresponding images for the recipe from your local hard drive.
3. Click the "Save Recipe" button to add the recipe to the database.

That's it! Your recipe will now be available to view and search for within the application.

## Repository
The source code for MyProject can be found on [GitLab](https://gitlab.com/BlackBeardedTaj/projects.git)

## Roadmap
* Add user authentication and registration functionality to the web application
* Implement a home page and cookbook view with search and filter options
* Develop a mobile application for the project
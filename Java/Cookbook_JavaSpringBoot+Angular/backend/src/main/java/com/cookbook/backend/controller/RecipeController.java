package com.cookbook.backend.controller;

import com.cookbook.backend.model.RecipeImages;
import org.apache.commons.io.FilenameUtils;

import com.cookbook.backend.model.Recipe;
import com.cookbook.backend.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/recipes")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @PostMapping(value = {"/recipes"}, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
//    public Recipe saveRecipe(@RequestParam("images") MultipartFile images, @ModelAttribute Recipe recipe) throws IOException {
    public Recipe saveRecipe(@RequestPart("recipe") Recipe recipe,
                             @RequestPart("images") MultipartFile[] file) {
        try {
            Set<RecipeImages> images = uploadImages(file);
            recipe.setImages(images);
            return recipeService.saveRecipe(recipe);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
//        if (images != null) {
//            byte[] imageBytes = images.getBytes();
//            recipe.setImages(imageBytes);
//            System.out.println(images);
//        }
//        return recipeService.saveRecipe(recipe);
    }

    public Set<RecipeImages> uploadImages(MultipartFile[] multipartFiles) throws IOException {
        Set<RecipeImages> images = new HashSet<>();

        for (MultipartFile file: multipartFiles) {
            RecipeImages recipeImages = new RecipeImages(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getBytes()
            );
            images.add(recipeImages);
        }

        return images;
    }


    @GetMapping
    public List<Recipe> getAllRecipes() {
        return recipeService.getAllRecipes();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Recipe> getRecipeById(@PathVariable Long id) {
        Optional<Recipe> recipe = recipeService.getRecipeById(id);
        if (recipe.isPresent()) {
            return ResponseEntity.ok(recipe.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRecipeById(@PathVariable Long id) {
        recipeService.deleteRecipeById(id);
        return ResponseEntity.noContent().build();
    }
}


package com.cookbook.backend.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "recipes")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;
    @Column(length = 1000)
    private String ingredients;
    @Column(length = 5000)
    private String preparation;
//    @Lob
//    @Column(name = "images", columnDefinition = "BLOB")
//    private byte[] images;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "recipes_and_recipe_images",
            joinColumns = {
                @JoinColumn(name = "recipe_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "image_id")
            }
    )
    private Set<RecipeImages> images;

//    public Recipe() {
//    }

//    public Recipe(String title, String ingredients, String preparation) {
//        this.title = title;
//        this.ingredients = ingredients;
//        this.preparation = preparation;
//    }
//
//    public Recipe(String title, String ingredients, String preparation, Set<RecipeImages> images) {
//        this.title = title;
//        this.ingredients = ingredients;
//        this.preparation = preparation;
//        this.images = images;
//    }

    // Constructors, getters, and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public Set<RecipeImages> getImages() {
        return images;
    }

    public void setImages(Set<RecipeImages> images) {
        this.images = images;
    }

    //    public byte[] getImages() {
//        return images;
//    }
//
//    public void setImages(byte[] images) {
//        this.images = images;
//    }
}

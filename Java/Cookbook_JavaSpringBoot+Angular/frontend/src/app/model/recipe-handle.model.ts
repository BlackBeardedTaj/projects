import { SafeUrl } from "@angular/platform-browser";

export interface RecipeHandle {
    file:File,
    url: SafeUrl
}
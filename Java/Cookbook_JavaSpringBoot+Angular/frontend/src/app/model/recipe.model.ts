import { RecipeHandle } from "./recipe-handle.model"

export class Recipe {
    constructor(
      // public id: number | null,
      public title: string,
      public ingredients: string,
      public preparation: string,
      public images: RecipeHandle[]
    ) {}
  }
  
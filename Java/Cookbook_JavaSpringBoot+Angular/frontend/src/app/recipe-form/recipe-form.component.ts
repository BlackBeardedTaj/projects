import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { RecipeService } from '../recipe.service';
import { Recipe } from '../model/recipe.model';
import { RecipeHandle } from '../model/recipe-handle.model';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.css']
})

// export class RecipeFormComponent {
export class RecipeFormComponent implements OnInit {

  recipe: Recipe = {
    // id: null,
    title: "",
    ingredients: "",
    preparation: "",
    images: [] 
  };

  // recipeForm!: NgForm;
  // submitted = false;
  // images: File | null = null;

  constructor(/*private formBuilder: FormBuilder, */private recipeService: RecipeService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    // this.recipeForm = this.formBuilder.group({
    //   title: ['', Validators.required],
    //   ingredients: ['', Validators.required],
    //   preparation: ['', Validators.required],
    //   images: ['', Validators.required]
    // });
  }

  // onFileSelected(event: any) {
  //   this.images = event.target.files[0] as File;
  // }

  addRecipe(recipeForm: NgForm) {
    console.log(this.recipe);

    const recipeFormData = this.prepareFormData(this.recipe)

    this.recipeService.createRecipe(recipeFormData).subscribe(
      (response: Recipe) => {
        console.log(response);
        recipeForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
    // this.submitted = true;

    // if (this.recipeForm.invalid) {
    //   return;
    // }
    
    // const formData = new FormData();
    // formData.append('title', this.recipeForm.get('title')!.value);
    // formData.append('ingredients', this.recipeForm.get('ingredients')!.value);
    // formData.append('preparation', this.recipeForm.get('preparation')!.value);
    // if (this.images) {
    //   formData.append('images', this.images, this.images.name);
    //   console.log(this.images["name"]);
    //   console.log(this.images);
    //   console.log("This.recipeForm: " + this.recipeForm.toString());
    // }
        
    // const recipe: Recipe = {
    //   id: null,
    //   title: this.recipeForm.get('title')!.value,
    //   ingredients: this.recipeForm.get('ingredients')!.value,
    //   preparation: this.recipeForm.get('preparation')!.value,
    //   images: this.recipeForm.get('images')!.value // You can set this to null or some default value, since the image data will be sent separately as part of the form data
    // };
    
    // this.recipeService.createRecipe(formData, recipe).subscribe(
    //   (createdRecipe: Recipe) => {
    //     console.log(createdRecipe);
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
    // this.recipeForm.reset();
    // console.log(recipe);
  }

  prepareFormData(recipe: Recipe): FormData {
    const formData = new FormData();
// 'recipe' and 'images' are taken from the backend, the same names that are used there
    formData.append(
      'recipe',
      new Blob([JSON.stringify(recipe)], {type: 'application/json'})
    );

    for(var i = 0; i < recipe.images.length; i++) {
      formData.append(
        'images',
        recipe.images[i].file,
        recipe.images[i].file.name
      );
    }
    return formData;
  }

  onFileSelected(event: Event) {
    const target = event.target as HTMLInputElement;
    if(target.files) {
      console.log(event);
      var recipeHandle: RecipeHandle;
      for (var i = 0; i < target.files.length; i++ ){
        const file = target.files[i];
        recipeHandle = {
          file: file,
          url: this.sanitizer.bypassSecurityTrustUrl(
            window.URL.createObjectURL(file)
          )
        }
        this.recipe.images.push(recipeHandle)
      }  
    }  /*console.log(event)*/
  }
  
  
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from './model/recipe.model';
import { tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  private baseUrl = 'http://localhost:8081/api/recipes';

  constructor(private http: HttpClient) { }

  public createRecipe(recipe: FormData) {
    console.log(recipe);

    return this.http.post<Recipe>(`${this.baseUrl}/recipes`, recipe);
  }
  
}

import * as THREE from 'three'; 


class Planet {
    constructor(name, texturePath, position, radius, sphereDetail) {
      this.name = name;
      this.texturePath = texturePath;
      this.position = position;
      this.radius = radius;
      this.sphereDetail = sphereDetail;
      this.lod = null;
      this.loaded = false;
    }
  
    load(callback) {
      const texture = new THREE.TextureLoader().load(this.texturePath, callback);
      this.material = new THREE.MeshStandardMaterial({ map: texture });
  
      this.lod = new THREE.LOD();
      this.sphereDetail.forEach(detail => {
        const geometry = new THREE.SphereBufferGeometry(this.radius, detail.segments, detail.segments);
        const mesh = new THREE.Mesh(geometry, this.material);
        mesh.distance = detail.distance;
        this.lod.addLevel(mesh, detail.distance);
      });
  
      this.lod.position.copy(this.position);
      this.loaded = true;
    }
  
    addToScene(scene) {
      if (this.loaded) {
        scene.add(this.lod);
      }
    }
  
    update(camera) {
      if (this.loaded) {
        const frustum = new THREE.Frustum();
        const cameraViewProjectionMatrix = new THREE.Matrix4();
        cameraViewProjectionMatrix.multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
        frustum.setFromProjectionMatrix(cameraViewProjectionMatrix);
  
        const sphere = new THREE.Sphere(this.position, this.radius);
        if (frustum.intersectsSphere(sphere)) {
          this.lod.update(camera);
        }
      }
    }
  }

  export { Planet }
  